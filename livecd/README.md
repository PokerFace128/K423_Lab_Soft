### LiveCD说明

这是我的第二个实验项目，还处于部分成功的状态。还有部分需要改进。WiKi也会进行更新。翻译也在进行中。

### 文件说明

- auto/  
    自动化脚本的存放。  
- config/  
    在构建过程中，你所需的部分。  
  - config/bootloaders  
    BIOS和UEFI的启动器，Debian原来的启动器不好看。就换成了Kali的。  
    当时在弄UEFI启动器的语言选项的时候不会弄跳转，只会跳到GRUB上。后面参考了Parrot的跳转。就把这问题给解决了。  
  - config/include.binary  
    isolinux的配置文件  
  - config/include.chroot  
    此处放入你的配置文件，后续会进行分桌面环境。现在展示的是xfce的配置。  
  - config/package-lists  
    把你需要的软件列表塞进去。格式为你的名称.list.chroot  
    示例:  
    K423.list.chroot
- - - 
### 后记：
    两年前就有这个制作LiveCD的想法，还在论坛找教程来看，虽然按照教程制作了一个，但在做实体机测试时却翻车了。那时候不知道有live-build。就扔一边没在管了。直到去年年底，再次翻看论坛帖子。才发现有新的教程，这时我才知道原来有live-build这工具。还有制作教程。但没有中文的，所以我得试试翻译看看。我也看到了日文版的。但有部分翻译看上去像是气到放弃治疗的样子。等我有时间学日语，就把日文翻译给修下。